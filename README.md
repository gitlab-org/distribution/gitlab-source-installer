# GitLab Source Installer

The GitLab Source Installer implements the [documented source installation
steps](https://docs.gitlab.com/ee/install/installation.html). It exists to
validate these steps and ensure changes in the various GitLab components
will install and function as expected.
